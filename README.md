## Setting up

### Get the config

```
git clone https://gitlab.com/vivo.fao/my-neovim-setup .config/nvim/
```

### Install plugins

Get Vim-Plug:

```
curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```

open nvim then:

```
:PlugInstall
```

Compile YCM:

```
cd .config/nvim/plugged/YouCompleteMe
python3 install.py --java-completer
```
