"==========PLUGIN BEGIN===========
call plug#begin('~/.config/nvim/plugged')
Plug 'junegunn/vim-easy-align'
Plug 'SirVer/ultisnips' | Plug 'honza/vim-snippets'
Plug  'KeitaNakamura/tex-conceal.vim'
Plug 'vimwiki/vimwiki'
Plug 'Valloric/YouCompleteMe'
Plug 'ervandew/supertab'
Plug 'mhinz/vim-startify'
Plug 'vim-airline/vim-airline' 
Plug 'vim-airline/vim-airline-themes'
Plug 'morhetz/gruvbox'
Plug 'vim-latex/vim-latex'
Plug 'vim-scripts/YankRing.vim'
Plug 'mattn/calendar-vim' 
Plug 'mzlogin/vim-markdown-toc'
Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() } }
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'tpope/vim-surround'
Plug 'vim-pandoc/vim-pandoc'
Plug 'vim-pandoc/vim-pandoc-syntax'
Plug 'turbio/bracey.vim', {'do': 'npm install --prefix server'}
Plug 'preservim/nerdtree' |
            \ Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'ryanoasis/vim-devicons'
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
"Plug 'scrooloose/nerdtree-project-plugin'
Plug 'PhilRunninger/nerdtree-visual-selection'
call plug#end()
"=========PLUGIN END=========
"=========OPTIONS BEGIN========
set ignorecase
set background=dark
set clipboard=unnamedplus
set smartcase
set expandtab
set relativenumber
let g:tex_flavor = "latex"
let g:tex_conceal = "abdmgs"
let g:vimwiki_conceallevel = 2
let g:pandoc#filetypes#pandoc_markdown = 0
filetype plugin on
filetype indent on
set showcmd
let mapleader = "\<Space>"
let vimDir = '$HOME/.config/nvim/'

if &diff
    colorscheme gruvbox
endif

"undo history across sessions
if has('persistent_undo')
        let myUndoDir = expand(vimDir . '/undodir')
        "Create dirs
        call system('mkdir ' . vimDir)
        call system('mkdir ' . myUndoDir)
        let &undodir = myUndoDir
        set undofile
endif
"move visual block
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv
"inoremap <C-Space> <esc>f\|li
"Matchparent colors
hi MatchParen cterm=bold, ctermfg=black
"========OPTIONS END========
"=========YCM BEGIN=========
let g:ycm_key_list_select_completion = ['<C-j>', '<Down>']
let g:ycm_key_list_previous_completion = ['<C-k>', '<Up>']
let g:SuperTabDefaultCompletionType = '<C-j>'
let g:ycm_min_num_of_chars_for_completion = 1
let g:ycm_filetype_blacklist = {
                        \ 'tagbar': 1,
                        \ 'qf': 1,
                        \ 'notes': 1,
                        \ 'unite': 1,
                        \ 'text': 1,
                        \ 'pandoc': 1,
                        \ 'infolog': 1,
                        \ 'mail': 1
                        \}
"========YCM END========
"========ULTISNIPS BEGIN=========
let g:UltiSnipsExpandTrigger = '<tab>'
let g:UltiSnipsJumpForwardTrigger = "<tab>"
let g:UltiSnipsJumpBackwardTrigger = "<s-tab>"
let g:UltiSnipsSnippetsDir ='~/.config/nvim/plugged/vim-snippets/UltiSnips'
autocmd FileType,BufFilePre,BufRead,BufNewFile,BufEnter *.md UltiSnipsAddFiletypes markdown.java.tex
autocmd FileType,BufFilePre,BufRead,BufNewFile,BufEnter *.vimwiki UltiSnipsAddFiletypes markdown.java.html.tex.texmath
"========ULTISNIPS END=========
"========LATEX SUITE BEGIN=========
let g:Tex_CustomTemplateDirectory='~/.config/nvim/plugged/vim-latex/ftplugin/latex-suite/templates/'
"========LATEX SUITE END=========
"========YANKRING BEGIN=========
nnoremap <silent> <F3> :YRShow<CR>
"========YANKRING END=========
"========VIMWIKI BEGIN=======
let g:vimwiki_table_mappings = 1


let g:vimwiki_list = [{'path':'/home/volkan/Dropbox/vimwiki/bachelor/',
                        \ 'path_html' : '/home/volkan/Dropbox/vimwiki/bachelor/html/',
                        \ 'template_path': '/home/volkan/Dropbox/vimwiki/my-vimwiki-for-university/templates/',
                        \ 'template_default': 'def_template',
                        \ 'template_ext' : '.html',
                        \ 'syntax' : 'markdown',
                        \ 'ext' : '.md',
                        \ 'custom_wiki2html': 'vimwiki_markdown',
                        \ 'list_margin': 0,
                        \ 'markdown_toc': 1, 
                        \ 'auto_export': 1},
                        \{'path':'/home/volkan/Dropbox/vimwiki/work/',
                        \ 'path_html' : '/home/volkan/Dropbox/vimwiki/work/html/',
                        \ 'template_path': '/home/volkan/Dropbox/vimwiki/my-vimwiki-for-university/templates/',
                        \ 'template_default': 'def_template',
                        \ 'template_ext' : '.html',
                        \ 'syntax' : 'markdown',
                        \ 'ext' : '.md',
                        \ 'custom_wiki2html': 'vimwiki_markdown',
                        \ 'list_margin': 0,
                        \ 'markdown_toc': 1, 
                        \ 'auto_export': 1},
                        \{'path':'/home/volkan/Dropbox/vimwiki/private/',
                        \ 'path_html' : '/home/volkan/Dropbox/vimwiki/private/html/',
                        \ 'template_path': '/home/volkan/Dropbox/vimwiki/my-vimwiki-for-university/templates/',
                        \ 'template_default': 'def_template',
                        \ 'template_ext' : '.html',
                        \ 'syntax' : 'markdown',
                        \ 'ext' : '.md',
                        \ 'custom_wiki2html': 'vimwiki_markdown',
                        \ 'list_margin': 0,
                        \ 'markdown_toc': 1, 
                        \ 'auto_export': 1}]
"==== pre tag for inserting code snippets
let g:vimwiki_valid_html_tags = 'b,i,s,u,sub,sup,kbd,br,hr, pre, script'
function! ToggleCalendar()
        execute ":Calendar"
        if exists("g:calendar_open")
                if g:calendar_open == 1
                        execute "q"
                        unlet g:calendar_open
                else
                        g:calendar_open = 1
                end
        else
                let g:calendar_open = 1
        end
endfunction
:autocmd FileType vimwiki map <Leader>c :call ToggleCalendar() <CR>
:autocmd FileType vimwiki map <Leader>d :VimwikiMakeDiaryNote <CR>
"========VIMWIKI END=======
"========MARKDOWN PREVIEW BEGIN=======
let g:mkdp_auto_start = 0
" normal/insert
" example
nmap <Leader>p :MarkdownPreview <CR>
"======= MARKDOWN PREVIEW END=====
"========VIM-AIRLINE BEGIN======
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#formatter = 'unique_tail'
let g:airline_powerline_fonts = 1
let g:airline_theme='bubblegum'
"========VIM-AIRLINE END========
"========YANKRING BEGIN=========
let g:yankring_history_dir = '~/.config/nvim/plugged/YankRing.vim/'
let g:yankring_clipboard_monitor=0
"=======YANKRING END========
"=======EASYALIGN BEGIN========
xmap ga <Plug>(EasyAlign)
nmap ga <Plug>(EasyAlign)
"=======EASYALIGN END=======
"=======MARKDOWN PREVIEW BEGIN=====
let g:mkdp_auto_start = 1
"=======MARDOWN PREVIEW END
"=======NERDTree BEGIN =====
nnoremap <leader>n :NERDTreeFocus<CR>
nnoremap <C-n> :NERDTree<CR>
nnoremap <C-t> :NERDTreeToggle<CR>
nnoremap <C-f> :NERDTreeFind<CR>
"=======NERDTree END ====
"=======GENERAL BEGIN=====
set tabstop=8 softtabstop=0 expandtab shiftwidth=4 smarttab
" Allow saving of files as sudo when I forgot to start vim using sudo.
cmap w!! w !sudo tee > /dev/null %

function! DoPrettyXML()
  " save the filetype so we can restore it later
  let l:origft = &ft
  set ft=
  " delete the xml header if it exists. This will
  " permit us to surround the document with fake tags
  " without creating invalid xml.
  1s/<?xml .*?>//e
  " insert fake tags around the entire document.
  " This will permit us to pretty-format excerpts of
  " XML that may contain multiple top-level elements.
  0put ='<PrettyXML>'
  $put ='</PrettyXML>'
  silent %!xmllint --format -
  " xmllint will insert an <?xml?> header. it's easy enough to delete
  " if you don't want it.
  " delete the fake tags
  2d
  $d
  " restore the 'normal' indentation, which is one extra level
  " too deep due to the extra tags we wrapped around the document.
  silent %<
  " back to home
  1
  " restore the filetype
  exe "set ft=" . l:origft
endfunction
command! PrettyXML call DoPrettyXML()
syn region math start=/\$\$/ end=/\$\$/
" inline math
syn match math '\$[^$].\{-}\$'
hi link math Statement
nnoremap th  :tabfirst<CR>
nnoremap tk  :tabnext<CR>
nnoremap tj  :tabprev<CR>
nnoremap tl  :tablast<CR>
nnoremap tt  :tabedit<Space>
nnoremap tn  :tabnext<Space>
nnoremap tm  :tabm<Space>
nnoremap td  :tabclose<CR>
" Alternatively use
"nnoremap th :tabnext<CR>
"nnoremap tl :tabprev<CR>
"nnoremap tn :tabnew<CR>
"======GENERAL END====
set nospell
